//
//  MyNativeViewWidget1.swift
//  Runner
//
//  Created by MacPro on 28/10/2023.
//

import Foundation
import Flutter
import UIKit

class MyNativeViewWidget1: NSObject, FlutterPlatformView{
    private var _view: UIView
    let nativeLabel = UILabel()
    private var methodChannel: FlutterMethodChannel
    
    init(frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?, binaryMessenger messenger: FlutterBinaryMessenger?) {
        _view = UIView()
        methodChannel = FlutterMethodChannel(name: "plugins/my_native_view", binaryMessenger: messenger!)
        super.init()
        
        createNativeView(view: _view)
        methodChannel.setMethodCallHandler(onMethodCallHandler)
    }
    
    func view()->UIView{
        return _view
    }
    
    func createNativeView(view _view: UIView){
        _view.backgroundColor = UIColor.purple
        
        nativeLabel.text = "View 2 IOS"
        nativeLabel.textColor = UIColor.yellow
        nativeLabel.textAlignment = .center
        nativeLabel.frame = CGRect(x: 0, y: 300, width: 180, height: 48)
        _view.addSubview(nativeLabel)
        
        
        let btn = UIButton()
        btn.backgroundColor = .blue
        btn.setTitle("Click", for: .normal)
        btn.addTarget(self, action: #selector(click), for: .touchUpInside)
        _view.addSubview(btn)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.centerXAnchor.constraint(equalTo: _view.centerXAnchor).isActive = true
        btn.topAnchor.constraint(equalTo: _view.safeAreaLayoutGuide.topAnchor, constant: 130).isActive = true
        
        
    }
    
    @objc func click(){
        methodChannel.invokeMethod("aaa", arguments: "Helllooooo")
    }
    
    func onMethodCallHandler(call: FlutterMethodCall, result: FlutterResult){
        switch (call.method){
        case "setData":
            updateView(txt: call.arguments as! String)
        default:
            result(FlutterMethodNotImplemented)
        }
    }
    
    func updateView(txt: String){
        nativeLabel.text = txt
    }
}
