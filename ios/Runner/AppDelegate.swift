import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
      
      weak var registrar = self.registrar(forPlugin: "plugins/native_view")
      let factory = MyNativeViewFactory(messager: registrar!.messenger())
      self.registrar(forPlugin:"plugins/view")!.register(factory, withId: "plugins/my_native_view")
      
      weak var registrar1 = self.registrar(forPlugin: "plugins/native_view1")
      let factory1 = MyNativeViewFactory1(messenger: registrar1!.messenger())
      self.registrar(forPlugin:"plugins/view1")!.register(factory1, withId: "plugins/my_native_view1")
      
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
