//
//  PlayerFactory.swift
//  Runner
//
//  Created by MacPro on 28/10/2023.
//

import Foundation
import Flutter
import UIKit

class PlayerFactory: NSObject, FlutterPlatformViewFactory{
    private var messenger: FlutterBinaryMessenger
    
    init(messenger: FlutterBinaryMessenger){
        self.messenger = messenger
        super.init()
    }
    
    func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?)-> FlutterPlatformView{
        return MyNativeViewWidget1(frame: frame, viewIdentifier: viewId, arguments: args, binaryMessenger: messenger)
    }
}
