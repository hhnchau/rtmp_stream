//
//  MyNativeViewFactory.swift
//  Runner
//
//  Created by MacPro on 21/10/2023.
//

import Foundation
import Flutter
import UIKit

class MyNativeViewFactory: NSObject, FlutterPlatformViewFactory{
    private var messager: FlutterBinaryMessenger
    
    init(messager: FlutterBinaryMessenger){
        self.messager = messager
        super.init()
    }
    
    func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?)-> FlutterPlatformView{
        return MyNativeViewWidget(
            frame: frame,
            viewIdentifier: viewId,
            arguments: args,
            binaryMessenger: messager
        )
    }
}
