//
//  MyNativeViewWidget.swift
//  Runner
//
//  Created by MacPro on 21/10/2023.
//

import Foundation
import Flutter
import UIKit


class MyNativeViewWidget: NSObject, FlutterPlatformView{
    private var _view: UIView

    init(frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?, binaryMessenger messenger: FlutterBinaryMessenger?){
        _view = UIView()
        super.init()

        createNativeView(view: _view)
    }

    func view() -> UIView{
        return _view
    }

    func createNativeView(view _view: UIView){
        //_view.backgroundColor = UIColor.blue
        let nativeLabel = UILabel()
        nativeLabel.text = "Hello iOS"
        nativeLabel.textColor = UIColor.red
        nativeLabel.textAlignment = .center
        nativeLabel.frame = CGRect(x: 0, y: 0, width: 180, height: 48)
        _view.addSubview(nativeLabel)
    }
}
