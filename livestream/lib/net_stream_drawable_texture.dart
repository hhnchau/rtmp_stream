import 'package:flutter/material.dart';

class NetStreamDrawableTexture extends StatefulWidget {
  const NetStreamDrawableTexture({super.key});

  @override
  State<StatefulWidget> createState() => _NetStreamDrawableState();

}

class _NetStreamDrawableState extends State<NetStreamDrawableTexture> {
  
  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {

  }
  
  Future<void> _updatePlatformState() async{
    
  }

  @override
  Widget build(BuildContext context) {
    return Texture(textureId: 1);
    
    return Container(
        color: Colors.black,
      );
  }
}
