
import 'livestream_platform_interface.dart';

class Livestream {
  Future<String?> getPlatformVersion() {
    return LivestreamPlatform.instance.getPlatformVersion();
  }
}
