import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'livestream_method_channel.dart';

abstract class LivestreamPlatform extends PlatformInterface {
  /// Constructs a LivestreamPlatform.
  LivestreamPlatform() : super(token: _token);

  static final Object _token = Object();

  static LivestreamPlatform _instance = MethodChannelLivestream();

  /// The default instance of [LivestreamPlatform] to use.
  ///
  /// Defaults to [MethodChannelLivestream].
  static LivestreamPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [LivestreamPlatform] when
  /// they register themselves.
  static set instance(LivestreamPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
