import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'livestream_platform_interface.dart';

/// An implementation of [LivestreamPlatform] that uses method channels.
class MethodChannelLivestream extends LivestreamPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('livestream');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
