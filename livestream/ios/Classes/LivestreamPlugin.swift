import Flutter
import UIKit

public class LivestreamPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "livestream", binaryMessenger: registrar.messenger())
    let instance = LivestreamPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch call.method {
    case "getPlatformVersion":
      result("iOS " + UIDevice.current.systemVersion)
    case "isToday":
        let arguments = call.arguments as! Dictionary<String, Any>
        let dateTime = arguments["dateTime"] as! String
        let localDate = dateTime.toDate(nil, region: Region.current)
        let checkToday = localDate?.isToday
        result(checkToday)
    default:
      result(FlutterMethodNotImplemented)
    }
  }
}
