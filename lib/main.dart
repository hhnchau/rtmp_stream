import 'dart:async';

//import 'package:audio_session/audio_session.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rtmp_stream/cache/reel_app_cache.dart';
import 'package:rtmp_stream/chewie_player.dart';
// import 'package:haishin_kit/audio_settings.dart';
// import 'package:haishin_kit/audio_source.dart';
// import 'package:haishin_kit/net_stream_drawable_texture.dart';
// import 'package:haishin_kit/rtmp_connection.dart';
// import 'package:haishin_kit/rtmp_stream.dart';
// import 'package:haishin_kit/video_settings.dart';
// import 'package:haishin_kit/video_source.dart';
// import 'package:permission_handler/permission_handler.dart';

//import 'package:player/player.dart';
//import 'package:livestream/livestream.dart';
import 'package:rtmp_stream/my_native_view.dart';
import 'package:rtmp_stream/my_native_view1.dart';
import 'package:rtmp_stream/my_listview.dart';
import 'package:rtmp_stream/my_orientation.dart';
import 'package:rtmp_stream/my_pageview.dart';
import 'package:rtmp_stream/my_player.dart';
import 'package:rtmp_stream/reels_player.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  //State<MyApp> createState() => MyListView(); //_MyAppState();
  //State<MyApp> createState() => MyPageView(); //_MyAppState();
  State<MyApp> createState() => MyPlayer();
  //State<MyApp> createState() => ChewiePlayer();
  //State<MyApp> createState() => ReelsPlayer();
  //State<MyApp> createState() => ReelsPlayerCache();
  //State<MyApp> createState() => MyOrientation();
}

class _MyAppState extends State<MyApp> {
  // RtmpConnection? _connection;
  // RtmpStream? _stream;
  bool _recording = false;
  // String _mode = "publish";
  // CameraPosition currentPosition = CameraPosition.back;

  String _platformVersion = 'Unknown';
  //final _demo = Livestream();

  static const platform = MethodChannel("plugins/my_native_view");

  @override
  void initState() {
    super.initState();
    initPlatformState();
    platform.setMethodCallHandler((MethodCall call) async{
      nativeListener(call.method, call.arguments);
    });
    initVersion();
  }

  Future<void> initVersion() async {
    //var string =   await _demo.getPlatformVersion() ?? 'unknow';
    setState(() {
      //_platformVersion = string;
    });
  }


  @override
  void dispose() {
    // _stream?.dispose();
    // _connection?.dispose();
    super.dispose();
  }

  Future<void> initPlatformState() async {
    // var status = await Permission.camera.request();
    // await Permission.microphone.request();

    // Set up AVAudioSession for iOS.
    // final session = await AudioSession.instance;
    // await session.configure(const AudioSessionConfiguration(
    //   avAudioSessionCategory: AVAudioSessionCategory.playAndRecord,
    //   avAudioSessionCategoryOptions:
    //   AVAudioSessionCategoryOptions.allowBluetooth,
    // ));

    // RtmpConnection connection = await RtmpConnection.create();
    // connection.eventChannel.receiveBroadcastStream().listen((event) {
    //   switch (event["data"]["code"]) {
    //     case 'NetConnection.Connect.Success':
    //       if (_mode == "publish") {
    //         _stream?.publish("live");
    //       } else {
    //         _stream?.play("live");
    //       }
    //       setState(() {
    //         _recording = true;
    //       });
    //       break;
    //   }
    // });
    //
    // RtmpStream stream = await RtmpStream.create(connection);
    // stream.audioSettings = AudioSettings(bitrate: 64 * 1000);
    // stream.videoSettings = VideoSettings(
    //   width: 480,
    //   height: 272,
    //   bitrate: 512 * 1000,
    // );
    // stream.attachAudio(AudioSource());
    // stream.attachVideo(VideoSource(position: currentPosition));
    //
    // if (!mounted) return;
    //
    // setState(() {
    //   _connection = connection;
    //   _stream = stream;
    // });
  }



  void nativeListener(String method, dynamic arguments){
    print(method);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('LiveStream: $_platformVersion'), actions: [
          IconButton(
            icon: const Icon(Icons.play_arrow),
            onPressed: () {
              // if (_mode == "publish") {
              //   _mode = "playback";
              //   _stream?.attachVideo(null);
              //   _stream?.attachAudio(null);
              // } else {
              //   _mode = "publish";
              //   //_stream?.attachAudio(AudioSource());
              //   //_stream?.attachVideo(VideoSource(position: currentPosition));
              // }
            },
          ),
          IconButton(
            icon: const Icon(Icons.flip_camera_android),
            onPressed: () {
              // if (currentPosition == CameraPosition.front) {
              //   currentPosition = CameraPosition.back;
              // } else {
              //   currentPosition = CameraPosition.front;
              // }
              // _stream?.attachVideo(VideoSource(position: currentPosition));
            },
          )
        ]),
        body: const Center(


          // child: _stream == null
          //     ? const Text("")
          //     : NetStreamDrawableTexture(_stream),

          child: Stack(
            children:[
              MyNativeView1(),
              MyNativeView()
            ],
          )


        ),
        floatingActionButton: FloatingActionButton(
          child: _recording
              ? const Icon(Icons.fiber_smart_record)
              : const Icon(Icons.not_started),
          onPressed: () {
            // if (_recording) {
            //   _connection?.close();
            //   setState(() {
            //     _recording = false;
            //   });
            // } else {
            //   _connection?.connect("rtmp://192.168.1.9/live");
            // }

            platform.invokeMethod("setData", "Clicked!!!!");


          },
        ),
      ),
    );
  }
}

//rtmp://user:password@192.168.1.200/live/user/nxtStream/
// void main() {
//   runApp(const MyApp());
// }
//
// class MyApp extends StatefulWidget {
//   const MyApp({Key? key}) : super(key: key);
//
//   @override
//   State<MyApp> createState() => _MyAppState();
// }
//
// class _MyAppState extends State<MyApp> {
//   late RtmpConnection _connection;
//   late RtmpStream _stream;
//
//   GlobalKey<NetStreamDrawableState> netStreamDrawableViewStateKey =
//   GlobalKey<NetStreamDrawableState>();
//
//   @override
//   void initState() {
//     super.initState();
//     initPlatformState();
//   }
//
//   Future<void> initPlatformState() async {
//     await Permission.camera.request();
//     await Permission.microphone.request();
//
//     _connection = await RtmpConnection.create();
//     _stream = await RtmpStream.create(_connection);
//     _stream.attachAudio(AudioSource());
//     _stream.attachVideo(VideoSource());
//
//     if (!mounted) return;
//
//     setState(() {
//       netStreamDrawableViewStateKey.currentState?.attachStream(_stream);
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: const Text('Plugin example app'),
//         ),
//         body: Center(
//           child: NetStreamDrawableView(key: netStreamDrawableViewStateKey),
//         ),
//         floatingActionButton: FloatingActionButton(
//           onPressed: () {
//             _connection.connect("rtmp://192.168.1.9/live");
//             _stream.publish("live");
//           },
//         ),
//       ),
//     );
//   }
// }



// cd ios
// rm Podfile.lock
// pod cache clean --all
// sudo arch -x86_64 pod install --repo-update
// pod repo update
// pod install