
import 'package:flutter/material.dart';
import 'package:rtmp_stream/main.dart';


class MyPageView extends State<MyApp> {
  PageController controller = PageController();
  final List<Widget> _list = <Widget>[
    const Center(
      child: Pages(
        text: 'Page One',
        color: Colors.teal,
      ),
    ),
    const Center(
      child: Pages(
        text: 'Page two',
        color: Colors.red,
      ),
    ),
    const Center(
      child: Pages(
        text: 'Page Three',
        color: Colors.green,
      ),
    )
  ];

  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Stack(
          children: [
            // PageView(
            //     allowImplicitScrolling: true,
            //     scrollDirection: Axis.horizontal,
            //     controller: controller,
            //     onPageChanged: (position){
            //       setState(() {
            //         _current = position;
            //       });
            //     },
            //     children: _list
            // ),
            ///
            // PageView.builder(
            //   itemBuilder: (context, index){
            //   return Pages(text: 'Page $index', color: Colors.redAccent);
            //   },
            //   controller: controller,
            //   itemCount: 10,
            // ),
            ///
            PageView.custom(
                childrenDelegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index){
                      return KeepAlive(
                          data: 'Keep $index'
                      );
                    }
                ),
              controller: controller,
              onPageChanged: (item){
                //print("Item $item");
              },
            ),
            Container(
              margin: const EdgeInsets.only(top: 50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  IconButton(onPressed: (){
                    print("Left Click");
                    controller.animateToPage(0, duration: const Duration(milliseconds: 300), curve: Curves.ease);
                  }, icon: const Icon(Icons.chevron_left, size: 60,)),
                  IconButton(onPressed: (){
                    print("Right Click");
                  }, icon: const Icon(Icons.chevron_right, size: 60,)),
                ],
              ),
            )
          ],
        )
      )
    );
  }
}

class Pages extends StatelessWidget{
  final String text;
  final Color color;
  const Pages({super.key, required this.text, required this.color});

  @override
  Widget build(BuildContext context) {
    print("========== $text ==========");
    return Container(
      color: color,
      child: Center(
        child: Column(
          mainAxisAlignment:  MainAxisAlignment.center,
          children: [
            Text(
              text,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.w400
              ),
            )
          ],
        ),
      ),
    );
  }
}

class KeepAlive extends StatefulWidget {
   const KeepAlive({super.key, required this.data});
   final String data;
   @override
   State<KeepAlive> createState() => _KeepAliveState();
}

class _KeepAliveState extends State<KeepAlive> with AutomaticKeepAliveClientMixin{
  @override
   bool get wantKeepAlive => true;

  @override
  void initState() {
    print("++++++++initState ${widget.data}");
    super.initState();
  }

   @override
   Widget build(BuildContext context) {
     print("==========Render Page ${widget.data} ==========");
     super.build(context);
     return Center(
        child: Text('HHHH ${widget.data}'),
     );
   }
}