import 'dart:math';

import 'package:flutter/material.dart';
import 'package:rtmp_stream/main.dart';
import 'package:video_player/video_player.dart';

class MyPlayer extends State<MyApp> {
  late VideoPlayerController _controller;
  var _progress = 0.0;
  var _state = '';

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.networkUrl(
      //Uri.parse('https://cfvod.livemalltv.com/live_test/u240221030158370-016/HLS/r240401085208658-001.m3u8'),
      Uri.parse('https://cflive.livestage.biz/r240404093106189-001/u240226110253301-002_r240404093106189-001/mylist.m3u8'),
      videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true),
    );

    _controller.addListener(() {
      _calculator();
      setState(() {});
      if (!_controller.value.isPlaying &&_controller.value.isInitialized &&
          (_controller.value.duration ==_controller.value.position)) { //checking the duration and position every time
        print("==+++++++END+++++++++");
        setState(() {
          _state = 'Video end';
        });
      }
    });
    //_controller.setLooping(true);
    _controller.initialize().then((value) => _controller.play());
  }

  _calculator() async {
    if(_controller.value.errorDescription != null){
      print("==+++++++ERROR+++++++++");
      setState(() {
        _state = 'An error occurred!!!';
      });
    }

    final position = await _controller.position;
    var current = position?.inSeconds ?? 0;
    final duration = _controller.value.duration;
    final total = duration.inSeconds ?? 0;

    final remained = max(0, total - current);
    final min = convertTwo(remained ~/60);
    final sec = convertTwo(remained%60);

    print("+++++++DURATION+++++++++" + min.toString() + ":"+ sec.toString());
    print("+++++++CURRENT+++++++++" + remained.toString());

    setState(() {
      _progress = position!.inMilliseconds.ceilToDouble() / duration.inMilliseconds.ceilToDouble();
    });
  }

  String convertTwo(int value){
    return value < 10 ? "0$value" : "$value";
  }

  Widget _slider(){
    return SliderTheme(
        data: SliderTheme.of(context).copyWith(
          activeTrackColor: Colors.red[700],
          inactiveTrackColor: Colors.red[100],
          trackShape: RoundedRectSliderTrackShape(),
          trackHeight: 2.0,
            thumbShape: RoundSliderThumbShape(
              enabledThumbRadius: 12
            ),
          thumbColor: Colors.redAccent,
          overlayColor: Colors.red.withAlpha(32),
          overlayShape: RoundSliderOverlayShape(overlayRadius: 2),
          tickMarkShape: RoundSliderTickMarkShape(),
          activeTickMarkColor: Colors.red[700],
          inactiveTickMarkColor: Colors.red[100],
          valueIndicatorShape: PaddleSliderValueIndicatorShape(),
          valueIndicatorColor: Colors.redAccent,
          valueIndicatorTextStyle: TextStyle(
            color: Colors.white
          )
        ),
      child: Slider(
        value: max(0, min(_progress * 100, 100)),
        min: 0,
        max: 100,
        divisions: 100,
        label: 80.toString().split(".")[0],
        onChanged: (value){
          setState(() {
            _progress = value * 0.01;
          });
        },
        onChangeStart: (value){
          _controller.pause();
        },
        onChangeEnd: (value){
          final duration = _controller?.value?.duration;
          if(duration != null){
            var newValue = max(0, min(value,99)) * 0.01;
            var mills = (duration.inMilliseconds * newValue).toInt();
            _controller?.seekTo(Duration(milliseconds: mills));
            _controller.play();
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.red,
            body: Stack(
              children: [
                Center(
                    child: AspectRatio(
                      aspectRatio: _controller.value.aspectRatio,
                      child: Stack(
                        children: [
                          VideoPlayer(_controller),
                          VideoProgressIndicator(_controller, allowScrubbing: false, padding: const EdgeInsets.all(10.0),),
                          const Text("INIT_VIDEO", style: TextStyle(color: Colors.blue, fontSize: 18)),
                        ],
                      ),
                    )
                ),
                Container(
                  margin: EdgeInsets.only(top: 100),
                    child: Row(
                      children: [
                        Text(_state, style: TextStyle(color: Colors.blue, fontSize: 18)),
                        Text(">>>>>"),
                        //_slider()
                      ],
                    )
                )
              ],
            )
        )
    );
  }
}