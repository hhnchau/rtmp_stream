import 'package:flutter/material.dart';
import 'package:rtmp_stream/main.dart';
import 'package:video_player/video_player.dart';

class ReelsPlayer extends State<MyApp> {
  late PageController _pageController;
  late List<VideoPlayerController> _playerControllers = [];

  var links = [
    'https://cfvod.livemalltv.com/live_test/u240221030158370-016/HLS/r240401085208658-001.m3u8'
    'https://cfvod.livemalltv.com/live_test/u240221030158370-016/HLS/r240401085208658-002.m3u8'
    'https://cfvod.livemalltv.com/u240201084740666-001/HLS/r240322041542540-003.m3u8',
    'http://playertest.longtailvideo.com/adaptive/wowzaid3/playlist.m3u8',
    'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8',
    'https://d1gnaphp93fop2.cloudfront.net/videos/multiresolution/rendition_new10.m3u8',
    'https://res.cloudinary.com/dannykeane/video/upload/sp_full_hd/q_80:qmax_90,ac_none/v1/dk-memoji-dark.m3u8',
    'https://diceyk6a7voy4.cloudfront.net/e78752a1-2e83-43fa-85ae-3d508be29366/hls/fitfest-sample-1_Ott_Hls_Ts_Avc_Aac_16x9_1280x720p_30Hz_6.0Mbps_qvbr.m3u8',
    'http://sample.vodobox.net/skate_phantom_flex_4k/skate_phantom_flex_4k.m3u8',
  ];

  @override
  void initState() {
    _pageController = PageController();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _initPlayer();
    });
    super.initState();
  }

  @override
  void dispose() {
    _releasePlayer();
    _pageController.dispose();
    super.dispose();
  }

  _initPlayer() async {
    // for (var link in links) {
    //   VideoPlayerController controller = VideoPlayerController.networkUrl(Uri.parse(link), videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true))
    //       ..initialize()
    //       .then((value) => setState(() {}));
    //   _playerControllers.add(controller);
    //
    //   controller.addListener(() {
    //     _updateProgress(controller);
    //   });
    // }

     _playerControllers = await Future.wait(links.map((link) => parse(link)));
     String s = '';

  }

  Future<VideoPlayerController> parse(String link) async{
    return await Future<VideoPlayerController>.value(VideoPlayerController.networkUrl(Uri.parse(link))..initialize().then((value) => setState(() {})));
  }

  _updateProgress(VideoPlayerController controller){
    int controllerIndex = _playerControllers.indexOf(controller);
    if(controllerIndex == -1) return;
    final progress = controller.value.position.inMilliseconds / controller.value.duration.inMilliseconds;

  }

  _loadMorePlayer() {
    print('[======LOAD-MORE======]');
    //_initPlayer();
  }

  _releasePlayer() {
    for (var controller in _playerControllers) {
      controller.removeListener(() { });
      controller.dispose();
    }


  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        surfaceTintColor: Colors.transparent,
        elevation: 0.0,
        actions: [],
      ),
      extendBodyBehindAppBar: true,
      body: PageView.builder(
        controller: _pageController,
        itemCount: _playerControllers.length,
        scrollDirection: Axis.vertical,
        pageSnapping: true,
        onPageChanged: (index){
          print('[======PAGE======]' + index.toString());

          for(var i=0;i<_playerControllers.length;i++){
            if(i != index){
              _playerControllers[i].pause();
            }else{
              _playerControllers[i].play();
            }
          }

          if(index == _playerControllers.length - 1){
            _loadMorePlayer();
          }
        },
        itemBuilder: (context, index) {
          final controller = _playerControllers[index];
          controller..play()..setLooping(true);
          return Stack(
            children: [
              controller.value.isInitialized
                  ? Center(
                      child: AspectRatio(
                      aspectRatio: controller.value.aspectRatio,
                      child: VideoPlayer(controller),
                    ))
                  : Container(
                      color: Colors.black87,
                      child: const Center(child: CircularProgressIndicator()),
                    ),
            ],
          );
        },
      ),
    ));
  }
}
