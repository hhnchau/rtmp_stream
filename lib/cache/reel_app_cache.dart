
import 'package:rtmp_stream/cache/custom_video_player/video_page.dart';
import 'package:rtmp_stream/cache/service/reel_service.dart';
import 'package:rtmp_stream/main.dart';
import 'package:flutter/material.dart';

//https://github.com/iamirzashowvik/flutter_reels_caching/tree/main
class ReelsPlayerCache extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: VideoReelPage(
        index: 0,
        reels: ReelService().getReels(),
      ),
    );
  }

}