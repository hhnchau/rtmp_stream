import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rtmp_stream/main.dart';


class MyOrientation extends State<MyApp> {

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          body: OrientationBuilder(builder: (_, orientation){
            if(orientation == Orientation.portrait){
              return Center(
                child: InkWell(
                  onTap: (){
                    SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
                  },
                  child: Text("PORTRAIT"),
                )
              );
            }else{
              return Center(
                child: InkWell(
                  onTap: (){
                    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
                  },
                  child: Text("LANDSCAPE"),
                )
              );
            }
          }),
        )
    );
  }

}