import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyNativeView1 extends StatefulWidget {
  const MyNativeView1({super.key});

  @override
  State<StatefulWidget> createState() => MyNativeViewState();
}

class MyNativeViewState extends State<MyNativeView1> {

  Map<String, dynamic> creationParams = <String, dynamic>{
    "chau": "Honag",
    "chau1": "Bao"
  };

  @override
  Widget build(BuildContext context) {
    if(defaultTargetPlatform == TargetPlatform.android){
      return AndroidView(viewType: "plugins/my_native_view1",
        creationParams: creationParams,
        layoutDirection: TextDirection.ltr,
        creationParamsCodec: const StandardMessageCodec(),
      );
    }else {
      return UiKitView(viewType: "plugins/my_native_view1",
        layoutDirection: TextDirection.ltr,
        creationParams: creationParams,
        creationParamsCodec: const StandardMessageCodec(),
      );
    }
  }
}
