
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:rtmp_stream/main.dart';
import 'package:video_player/video_player.dart';

class ChewiePlayer extends State<MyApp> {
  late VideoPlayerController _controller;
  late ChewieController _chewieController;


  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.networkUrl(
      Uri.parse('https://cfvod.livemalltv.com/HLS/vietnam_landscape.m3u8'),
      videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true),
    );

    _controller.initialize().then((value) => setState(
            () => _chewieController = ChewieController(videoPlayerController: _controller, aspectRatio: _controller.value.aspectRatio)
    ));
  }

  @override
  void dispose() {
    _controller.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            backgroundColor: Colors.red,
            body: Stack(
              children: [
                Center(
                    child: AspectRatio(
                      aspectRatio: _controller.value.aspectRatio,
                      child: Chewie(controller: _chewieController)
                    )
                ),
              ],
            )
        )
    );
  }
}