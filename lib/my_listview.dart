import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rtmp_stream/main.dart';
import 'package:rtmp_stream/shimmer.dart';

class MyListView extends State<MyApp> {
  final urlImages = [
    'https://devapi.livestage.biz/files/livefile/r231201031909472-009.jpg',
    'https://devapi.livestage.biz/files/livefile/r231205091943216-003.jpg',
    'https://devapi.livestage.biz/files/userphoto/u231129084050555-001/f231129084414388-001.jpg',
    'https://devapi.livestage.biz/files/livefile/r231204153204140-017.jpg',
    'https://devapi.livestage.biz/files/livefile/r231204153204140-017.jpg',
    'https://devapi.livestage.biz/files/livefile/r231205091943216-002.jpg',
    'https://devapi.livestage.biz/files/livefile/r231204153204140-018.jpg',
    'https://devapi.livestage.biz/files/livefile/r231204153204140-010.jpg',
    'https://devapi.livestage.biz/files/livefile/r231204153204140-009.jpg',
    'https://devbeacon.livestage.biz/images_test/APP_IMAGE/95/102/U_95_102_20231208031222_521f9e17-1191-46bb-b484-bf90dcd4a359.jpg',
    'https://devbeacon.livestage.biz/images_test/APP_IMAGE/95/102/U_95_102_20231208031402_f998ad22-06f0-4fcd-94c2-814d753b5dcc.jpg',
    'https://devbeacon.livestage.biz/images_test/APP_IMAGE/95/102/U_95_102_20231208065023_fe8215d2-6fee-4c9b-811b-35e06690b25e.gif',
    'https://devbeacon.livestage.biz/images_test/APP_IMAGE/95/102/U_95_102_20231208035013_7cfb07e1-fd6b-4029-aa32-c29e9237fc90.webp',
    'https://devbeacon.livestage.biz/images_test/APP_IMAGE/95/102/U_95_102_20231208035155_d4e30c52-d452-491a-8969-fa0b8c8b4527.jpg',
    'https://devbeacon.livestage.biz/images_test/APP_IMAGE/95/102/U_95_102_20231208035844_2505d955-454c-4869-906b-0f3b216afef3.jpg',
    'https://devbeacon.livestage.biz/images_test/APP_IMAGE/95/102/U_95_102_20231208063734_0a462270-7383-486e-9ad1-04464103de2d.png',
    'https://devbeacon.livestage.biz/images_test/APP_IMAGE/95/102/U_95_102_20231208063925_297984b4-478f-43b0-96f6-6546333c052a.jpg',
    'https://cdn.pixabay.com/photo/2023/04/22/10/28/sheep-7943526_1280.jpg',
    'https://cdn.pixabay.com/photo/2023/09/28/02/54/mallard-8280561_1280.jpg',
    'https://cdn.pixabay.com/photo/2023/09/18/13/46/spider-8260620_1280.jpg',
    'https://cdn.pixabay.com/photo/2023/07/29/17/36/fly-8157417_1280.jpg',
    'https://cdn.pixabay.com/photo/2023/09/17/21/08/geneva-8259296_1280.jpg',
    'https://cdn.pixabay.com/photo/2023/11/21/21/38/puffins-8404284_1280.jpg',
    'https://cdn.pixabay.com/photo/2023/12/05/16/57/dog-8432098_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/10/01/09/21/pets-3715733_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/12/13/05/15/puppy-1903313_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/11/14/04/45/elephant-1822636_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/01/14/12/59/iceland-1979445_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/08/12/16/59/parrot-3601194_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/05/31/18/38/sea-2361247_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/02/07/16/47/kingfisher-2046453_1280.jpg',
    'https://cdn.pixabay.com/photo/2015/11/16/16/28/bird-1045954_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/05/08/13/15/bird-2295431_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/05/08/13/15/bird-2295436_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/02/28/23/00/swan-2107052_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/08/12/17/11/roe-deer-2634729_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/12/10/15/16/white-horse-3010129_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/11/21/17/09/animals-1846546_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/01/28/11/00/white-tailed-eagle-2015098_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/05/21/15/14/balloons-2331488_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/12/12/01/33/seagull-1900657_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/11/14/04/00/beach-1822544_1280.jpg',
    'https://cdn.pixabay.com/photo/2015/06/19/21/33/seagulls-815304_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/11/22/23/53/starfish-1851289_1280.jpg',
    'https://cdn.pixabay.com/photo/2013/10/09/02/27/rocks-192988_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/01/31/16/27/sea-3121435_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/10/10/22/05/jellyfishes-1730018_1280.jpg',
    'https://cdn.pixabay.com/photo/2019/08/06/12/15/beach-4388225_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/01/05/02/47/fishing-3062034_1280.jpg',
    'https://cdn.pixabay.com/photo/2015/09/22/19/00/ship-952292_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/04/05/14/10/boat-3292934_1280.jpg',
    'https://cdn.pixabay.com/photo/2014/05/27/15/22/deckchairs-355596_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/05/06/14/00/water-3378639_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/11/21/13/58/ball-1845546_1280.jpg',
    'https://cdn.pixabay.com/photo/2019/03/01/18/52/house-4028391_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/06/26/15/00/beach-2444040_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/07/07/18/10/island-2482200_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/06/04/19/43/lighthouse-2372004_1280.jpg',
    'https://cdn.pixabay.com/photo/2013/11/07/21/04/boat-207129_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/02/25/05/14/shipwreck-2096945_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/08/14/18/27/sailing-boat-1593613_1280.jpg',
    'https://cdn.pixabay.com/photo/2014/07/19/06/48/digital-art-396825_1280.png',
    'https://cdn.pixabay.com/photo/2014/11/01/18/46/dubrovnik-512798_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/03/05/00/34/panorama-2117310_1280.jpg',
    'https://cdn.pixabay.com/photo/2014/08/12/00/01/santorini-416135_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/08/08/15/08/cruise-1578528_1280.jpg',
    'https://cdn.pixabay.com/photo/2015/03/11/12/31/buildings-668616_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/02/25/17/38/george-washington-bridge-2098351_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/03/02/17/19/paris-3193674_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/06/22/11/54/town-2430571_1280.jpg',
    'https://cdn.pixabay.com/photo/2013/09/14/19/53/city-182223_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/04/25/09/26/eiffel-tower-3349075_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/01/19/19/26/amsterdam-1150319_1280.jpg',
    'https://cdn.pixabay.com/photo/2014/11/13/23/34/palace-530055_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/02/04/17/11/venice-3130323_1280.jpg',
    'https://cdn.pixabay.com/photo/2021/08/30/21/29/port-6587129_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/08/08/14/44/tower-2611573_1280.jpg',
    'https://cdn.pixabay.com/photo/2015/11/18/16/03/valencia-1049389_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/07/13/03/15/paris-2499022_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/01/09/12/20/hamburg-3071437_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/07/18/20/25/channel-3547224_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/08/07/10/22/buildings-2602324_1280.jpg',
    'https://cdn.pixabay.com/photo/2013/04/18/14/39/ship-105596_1280.jpg',
    'https://cdn.pixabay.com/photo/2013/05/15/09/12/tourist-attraction-111329_1280.jpg',
    'https://cdn.pixabay.com/photo/2018/02/26/14/22/venice-3183168_1280.jpg',
    'https://cdn.pixabay.com/photo/2014/11/10/20/07/bridge-525857_1280.jpg',
    'https://cdn.pixabay.com/photo/2014/09/11/18/23/tower-bridge-441853_1280.jpg',
    'https://cdn.pixabay.com/photo/2020/12/15/15/33/building-5834006_1280.jpg',
    'https://cdn.pixabay.com/photo/2020/06/18/11/27/landscape-5313115_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/11/23/15/44/buildings-1853632_1280.jpg',
    'https://cdn.pixabay.com/photo/2014/02/17/10/20/statue-of-liberty-267948_1280.jpg',
    'https://cdn.pixabay.com/photo/2019/06/06/13/36/italy-4256018_1280.jpg',
    'https://cdn.pixabay.com/photo/2020/07/12/16/40/paris-5397889_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/09/23/17/05/bridge-1690078_1280.jpg',
    'https://cdn.pixabay.com/photo/2020/08/09/11/31/business-5475283_1280.jpg',
    'https://cdn.pixabay.com/photo/2020/04/19/23/51/forsythia-5065900_1280.jpg',
    'https://cdn.pixabay.com/photo/2014/04/17/05/16/namdaemun-326138_1280.jpg',
    'https://cdn.pixabay.com/photo/2019/04/20/11/33/korea-4141535_1280.jpg',
    'https://cdn.pixabay.com/photo/2019/07/12/05/39/sejong-city-4331956_1280.jpg',
    'https://cdn.pixabay.com/photo/2017/06/23/17/04/train-2435279_1280.jpg',
    'https://cdn.pixabay.com/photo/2023/09/22/11/10/lisbon-8268841_1280.jpg',
    'https://cdn.pixabay.com/photo/2022/04/22/20/54/assisi-city-7150611_1280.jpg'
  ];

  bool isLoading = true;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      loadData();
    });
  }

  Future loadData() async {
    setState(() => isLoading = true);

    await Future.wait(
        urlImages.map((image) => cacheImage(context, image)).toList());

    setState(() => isLoading = false);
  }

  Future cacheImage(BuildContext context, String urlImage) {
    return precacheImage(CachedNetworkImageProvider(urlImage), context);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
          body: ListView.separated(
              itemBuilder: (context, index) {
                final urlImage = urlImages[index];
                return widgetWithoutCache(urlImage, index);
                //return widgetCache(urlImage, index);
                //return blurImage(urlImage, index);
              },
              separatorBuilder: (context, index) {
                return const Divider();
              },
              itemCount: urlImages.length)),
    );
  }
  
  Widget blurImage(String urlImage, int index){
    return Container(
      width: 100,
      height: 100,
      decoration:  BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(urlImage),
        ),
      ),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 0.0, sigmaY: 0.0),
        child: Container(
          color: Colors.black.withOpacity(0.1),
        ),
      ),
    );
  }

  Widget widgetWithoutCache(String urlImage, int index) {
    return Column(
      children: [
        SizedBox(
          height: 200,
          child: Image.network(
            urlImage,
            cacheHeight: 800,
            cacheWidth: 800,
            loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? progress){
              if(progress == null) {
                return child;
              }else{
                return Shimmer.fromColors(
                  baseColor: Colors.grey.shade300,
                  highlightColor: Colors.grey.shade100,
                  child: Container(
                    height: 200,
                    width: 200,
                    color: Colors.grey,
                  ),
                );
                //   const CircularProgressIndicator(
                //   strokeWidth: 1,
                //   valueColor:  AlwaysStoppedAnimation<Color>(Colors.red)
                // );
              }
            },
            errorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace){
              return Text("");
            },
          ),
        ),
        Text(
          'Image ${index + 1}, Image ${index + 1}, Image ${index + 1}, Image ${index + 1}, Image${index + 1}, Image ${index + 1}',
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        )
      ],
    );
  }

  Widget widgetCache(String urlImage, int index) {
    return Column(
      children: [
        CircleAvatar(
          backgroundImage: CachedNetworkImageProvider(urlImage),
          radius: 100,
        ),
        Text(
          'Image ${index + 1}, Image ${index + 1}, Image ${index + 1}, Image ${index + 1}, Image${index + 1}, Image ${index + 1}',
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        )
      ],
    );
  }
}
