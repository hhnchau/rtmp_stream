package com.example.rtmp_stream

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        flutterEngine.platformViewsController.registry.registerViewFactory(
            "plugins/my_native_view",
            MyNativeViewFactory(flutterEngine.dartExecutor.binaryMessenger)
        )

        flutterEngine.platformViewsController.registry.registerViewFactory(
            "plugins/my_native_view1",
            MyNativeView1Factory()
        )
    }
}
