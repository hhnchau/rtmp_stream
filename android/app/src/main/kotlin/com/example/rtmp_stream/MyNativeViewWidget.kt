package com.example.rtmp_stream

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.platform.PlatformView

class MyNativeViewWidget internal  constructor(context: Context, id: Int, messenger: BinaryMessenger, data: String): PlatformView, MethodCallHandler {
    private var view: View = LayoutInflater.from(context).inflate(R.layout.my_view_native, null)
    private val methodChannel: MethodChannel = MethodChannel(messenger, "plugins/my_native_view")
    private val txt: TextView
    override fun getView(): View {
        return view
    }

    init {
        methodChannel.setMethodCallHandler(this)
        txt = view.findViewById(R.id.text)

        var btn: Button = view.findViewById(R.id.btn)
        btn.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p: View?) {
                methodChannel.invokeMethod("aaa", data, null)
            }
        })
    }

    override fun dispose() {

    }

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when(call.method){
            "setData" -> setData(call, result)
            else -> result.notImplemented()
        }
    }

    private fun setData(call: MethodCall, result: MethodChannel.Result){
        val data: String = call.arguments as String

        txt.text = data

        result.success(null)
    }



}