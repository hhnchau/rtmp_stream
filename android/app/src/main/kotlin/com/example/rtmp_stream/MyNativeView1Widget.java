package com.example.rtmp_stream;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import io.flutter.plugin.platform.PlatformView;

public class MyNativeView1Widget implements PlatformView {
    private final TextView textView;

    public MyNativeView1Widget(Context context, String s) {
        textView = new TextView(context);
        textView.setBackgroundColor(Color.CYAN);
        textView.setText(s);
    }

    @Nullable
    @Override
    public View getView() {
        return textView;
    }

    @Override
    public void dispose() {

    }
}
